package com.b2wdigital.test.pyramid;


import com.b2wdigital.test.pyramid.core.BadPyramidException;
import com.b2wdigital.test.pyramid.core.Pyramid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Application {

    public static void main (String[]  args) {
        if (args == null || args.length == 0) throw new IllegalArgumentException("Bad input parameter.");
        if (args[0] == null) throw new IllegalArgumentException("Bad input parameter");
        Application application = new Application();
        application.init(args[0].replace(" ", ""));
    }

    private void init(String inputData) {
        String pyramidRows;
        pyramidRows = parseInput(inputData);

        if (pyramidRows == "") throw new IllegalArgumentException("Bad input parameter.");

        Integer[][] pyramidArray = parsePyramidRows(pyramidRows);

        Pyramid pyramid = new Pyramid(pyramidArray);
        try {
            System.out.println(pyramid.getMaximumTotal());
        } catch (BadPyramidException e) {
            System.out.println(e.getMessage());
        }
    }

    protected String parseInput(String pyramidArray) {
        String regex = "(\\[|\\{)(.+)(\\]|\\})";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(pyramidArray);
        if (matcher.find()) {
            return matcher.group(2);
        }
        return "";
    }

    protected Integer[][] parsePyramidRows (String pyramidRows) {
        List<Integer[]> rowsList = new ArrayList<>();
        String regex = "((\\[|\\{)(((\\d+),?)+)(\\]|\\}))";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(pyramidRows);
        while (matcher.find()) {
            String group = matcher.group(3);
            Integer[] row = Arrays.stream( Stream.of(group.split(",")).mapToInt(Integer::parseInt).toArray() ).boxed().toArray(Integer[]::new);
            rowsList.add(row);
        }
        Integer[][] pyramidArray = new Integer[rowsList.size()][];
        for (int i = 0; i < pyramidArray.length; i++) {
            pyramidArray[i] = rowsList.get(i);
        }
        return pyramidArray;
    }
}

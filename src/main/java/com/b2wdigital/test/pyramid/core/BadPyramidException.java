package com.b2wdigital.test.pyramid.core;

public class BadPyramidException extends Exception {

    public BadPyramidException() {
    }

    public BadPyramidException(String message) {
        super(message);
    }

    public BadPyramidException(Throwable cause) {
        super(cause);
    }

    public BadPyramidException(String message, Throwable cause) {
        super(message, cause);
    }

}

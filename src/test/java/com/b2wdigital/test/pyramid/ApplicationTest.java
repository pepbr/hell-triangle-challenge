package com.b2wdigital.test.pyramid;

import com.b2wdigital.test.pyramid.Application;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;

@RunWith (JUnit4.class)
public class ApplicationTest {

//    private final static Logger logger = Logger.getLogger(ApplicationTest.class);

    @Test (expected = IllegalArgumentException.class)
    public void testNoInputParameters() throws Throwable{
        String[] args = null;
        Application.main( args );
    }

    @Test (expected = IllegalArgumentException.class)
    public void testEmptyInputParameters() throws Throwable{
        String[] args = { "" };
        Application.main( args );
    }

    @Test (expected = IllegalArgumentException.class)
    public void testBadStringInputParameters() throws Throwable{
        String[] args = { "this should end in error" };
        Application.main( args );
    }

    @Test ()
    public void testInputParser() throws Throwable{
        Application application = new Application();
        String pyramidArray = "[[6],[3,5],[9,7,1],[4,6,8,4]]";
        String expectedResult = "[6],[3,5],[9,7,1],[4,6,8,4]";
        String gotResult = application.parseInput(pyramidArray);
        Assert.assertTrue("Bad input string - expected: " + expectedResult + "   got: " + gotResult, gotResult.equals(expectedResult));

        pyramidArray = "{{6},{3,5},{9,7,1},{4,6,8,4}}";
        expectedResult = "{6},{3,5},{9,7,1},{4,6,8,4}";
        gotResult = application.parseInput(pyramidArray);
        Assert.assertTrue("Bad input string - expected: " + expectedResult + "   got: " + gotResult, gotResult.equals(expectedResult));
    }

    @Test ()
    public void testPyramidArrayParser() throws Throwable{
        Application application = new Application();
        String pyramidRows = "[6],[3,5],[9,7,1],[4,6,8,4]";
        Integer[][] expectedResult = {{6},{3,5},{9,7,1},{4,6,8,4}};
        Integer[][] gotResult = application.parsePyramidRows(pyramidRows);
        Assert.assertTrue("Bad pyramid rows", Arrays.deepEquals(gotResult, expectedResult));
    }

}
